import React from 'react'

const ArrowIcon = () => {
  return (
    <svg
      focusable="false"
      viewBox="0 0 24 24"
      aria-hidden="true"
      role="presentation"
    >
      <path d="M7 10l5 5 5-5z" />
    </svg>
  )
}

export default ArrowIcon
