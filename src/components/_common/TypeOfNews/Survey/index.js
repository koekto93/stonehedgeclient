//@flow
import React from 'react'

import Image from '../../Image'
import PreviewText from '../PreviewText'
import NewsControl from '../NewsControl'
import SurveyQuestions from './SurveyQuestions'

import { Container } from './styled'

type newsDataType = {
  id: string,
  theme: string,
  newsType: string,
  shortText: string,
  fullText: string,
  date: string,
  imgSrc: string,
  image: boolean,
  questions: Array<any>, // TODO Расписать типы
}
type SurveyProps = {
  newsData: newsDataType,
}

const openNewsId = '212232111'

const Survey = ({ newsData }: SurveyProps) => {
  return (
    <Container>
      <div
        className={`preview-wrapper ${
          newsData.id !== openNewsId ? 'preview-wrapper_news-closed' : ''
        }`}
      >
        <div className={`${openNewsId === newsData.id ? 'news_opened' : ''}`}>
          <Image
            src={newsData.imgSrc}
            width="280px"
            height="139px"
            alt="surveyImg"
          />
        </div>
        <div className={newsData.image ? 'preview-text_with-image' : ''}>
          <PreviewText theme={newsData.theme} shortText={newsData.shortText} />
        </div>
        <NewsControl type={newsData.newsType} date={newsData.date} />
      </div>
      {openNewsId === newsData.id && (
        <SurveyQuestions
          questions={newsData.questions}
          imgSrc={newsData.imgSrc}
        />
      )}
    </Container>
  )
}

export default Survey
