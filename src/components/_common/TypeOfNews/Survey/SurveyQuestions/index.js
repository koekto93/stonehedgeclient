import React from 'react'
import { Form, Field } from 'react-final-form'

import CheckboxGrp from './TypeOfQuestions/CheckboxGrp'
import TextBox from './TypeOfQuestions/TextBox'
import RadioGrp from './TypeOfQuestions/RadioGrp'
import Image from '../../../Image'

import { Container } from './styled'

type SurveyQuestionsProps = {
  questions: Array<any>, //расписать все возможные вопросы
  imgSrc: string,
}

const sleep = ms => new Promise(resolve => setTimeout(resolve, ms))

const onSubmit = async values => {
  await sleep(300)
  window.alert(JSON.stringify(values, 0, 2))
}

const SurveyQuestions = ({ questions, imgSrc }: SurveyQuestionsProps) => {
  function createQuestion(question, index) {
    const { type } = question
    if (type === 'radio')
      return (
        <Field
          name={`question ${index + 1}`}
          questionData={question}
          key={index}
          questionNumber={index}
          component={RadioGrp}
        />
      )
    if (type === 'checkbox')
      return (
        <Field
          name={`question ${index + 1}`}
          questionData={question}
          key={index}
          questionNumber={index}
          component={CheckboxGrp}
        />
      )
    if (type === 'textbox')
      return (
        <Field
          name={`question ${index + 1}`} //нужно подумать по поводу названий полей
          questionData={question}
          key={index}
          questionNumber={index}
          component={TextBox}
        />
      )
  }
  const arrOfQuestions = questions.map((question, index) =>
    createQuestion(question, index)
  ) //переделать key

  return (
    <Container>
      <Image src={imgSrc} width="704px" height="398px" alt="surveyImg" />

      <div className="survey-control-type">
        В целях улучшения сервиса просим вас пройти опрос
      </div>

      <Form
        onSubmit={onSubmit}
        render={({ handleSubmit, form, submitting, pristine, values }) => (
          <form onSubmit={handleSubmit}>
            <div>{arrOfQuestions}</div>
            <div className="button">
              <button type="submit" className="submit-button">
                Готово
              </button>
            </div>
          </form>
        )}
      />
    </Container>
  )
}
export default SurveyQuestions
