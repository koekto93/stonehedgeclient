import styled from 'styled-components'

export const Container = styled.div`
  margin-top: 15px;
  & .survey-control-type {
    color: #1b1e2d;
    font-family: PFEncoreSansPro-Light;
    font-size: 16px;
    font-weight: 300;
    letter-spacing: 0.21px;
    line-height: 22px;
    margin-bottom: 10px;
  }

  & .submit-button {
    border: 1px solid #aaaaaa;
    border-radius: 4px;
    background-color: #ffffff;

    color: #7c7c7c;
    font-family: PFEncoreSansPro-Book;
    font-size: 12px;
    letter-spacing: 0.53px;
    line-height: 14px;

    padding: 8px 60px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
  }
`
