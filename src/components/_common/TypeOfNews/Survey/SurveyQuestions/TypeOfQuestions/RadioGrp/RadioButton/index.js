import React from 'react'

/* type SurveyBodyProps = {
  questions: Array<any>,
} */
import { Container } from './styled'

const RadioButton = ({ onChange, name, value }) => {
  return (
    <Container>
      <label className="radio">
        <input type="radio" onChange={onChange} name={name} value={value} />
        <span className="radio__text">{value}</span>
      </label>
    </Container>
  )
}

export default RadioButton
