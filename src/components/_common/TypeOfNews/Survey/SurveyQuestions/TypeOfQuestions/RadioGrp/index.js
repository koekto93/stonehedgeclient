import React from 'react'

import RadioButton from './RadioButton'

import { Container } from './styled'

/* type SurveyBodyProps = {
  questions: Array<any>,
} */

const RadioGrp = ({ input, questionNumber, questionData }) => {
  const allOptions = questionData.options.map((option, i) => (
    <RadioButton
      key={i}
      value={option}
      name={`radioGrp${questionNumber + 1}`}
      onChange={event => input.onChange(event.target.value)}
    />
  ))
  return (
    <Container>
      <div className="radio-group-header-text">{`${questionNumber + 1}. 
          ${questionData.text}`}</div>
      <div className="radio-group-options-wrapper">{allOptions}</div>
    </Container>
  )
}

export default RadioGrp
