import styled from 'styled-components'

export const Container = styled.div`
  margin-bottom: 15px;
  & .textbox-header-text {
    color: #1b1e2d;
    font-family: PFEncoreSansPro-Medium;
    font-size: 14px;
    line-height: 20px;
    opacity: 0.97;
    margin-bottom: 20px;
  }

  & .textbox-input {
    width: 360px;
    height: 81.76px;
    padding: 5px 7px;
    box-sizing: border-box;
    border: 1px solid #ececec;
    border-radius: 2px;
    font-family: PFEncoreSansPro-Light;
    font-size: 14px;
    color: #1b1e2d;
    resize: none;
  }
`
