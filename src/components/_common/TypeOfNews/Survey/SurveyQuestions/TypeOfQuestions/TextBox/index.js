import React from 'react'

import { Container } from './styled'

/* type SurveyBodyProps = {
  questions: Array<any>,
} */

class TextBox extends React.Component {
  render() {
    const { input, questionData, questionNumber } = this.props
    return (
      <Container>
        <div className="textbox-header-text">{`${questionNumber + 1}. ${
          questionData.text
        }`}</div>

        <textarea
          className="textbox-input"
          type="text"
          name="stooge"
          placeholder="Ваш ответ"
          onChange={event => input.onChange(event.target.value)}
        />
      </Container>
    )
  }
}

export default TextBox
