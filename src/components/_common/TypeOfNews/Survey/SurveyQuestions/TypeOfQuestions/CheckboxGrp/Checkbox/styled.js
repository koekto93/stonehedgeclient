import styled from 'styled-components'

export const Container = styled.div`
  margin-bottom: 8px;
  & .checkbox {
    display: inline-block;
    position: relative;
    padding-left: 26px;
    margin-bottom: 10px;
    cursor: pointer;
    font-size: 22px;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    font-family: PFEncoreSansPro-Book;
    font-size: 14px;
    color: #1b1e2d;
  }

  & .checkbox input {
    position: absolute;
    opacity: 0;
    cursor: pointer;
    height: 0;
    width: 0;
  }

  & .checkbox__text {
    position: absolute;
    top: 0;
    left: 0;
    width: 16px;
    height: 15.76px;
    background-color: ffffff#;
    border-radius: 2px;
    border: 1px solid #d2d2d2;
  }

  & .checkbox:hover input ~ .checkbox__text {
    background-color: #ccc;
  }

  & .checkbox input:checked ~ .checkbox__text {
    background-color: #8e97a8;
    border: 1px solid #8e97a8;
  }

  & .checkbox__text:after {
    content: '';
    position: absolute;
    display: none;
  }

  & .checkbox input:checked ~ .checkbox__text:after {
    display: block;
  }

  & .checkbox .checkbox__text:after {
    left: 6px;
    top: 2px;
    width: 4px;
    height: 8px;
    border: solid white;
    border-width: 0 1px 1px 0;
    -webkit-transform: rotate(45deg);
    -ms-transform: rotate(45deg);
    transform: rotate(45deg);
  }
`
