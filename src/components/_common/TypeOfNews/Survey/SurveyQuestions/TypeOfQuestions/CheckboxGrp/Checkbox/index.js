import React from 'react'

/* type SurveyBodyProps = {
  questions: Array<any>,
} */
import { Container } from './styled'

const Checkbox = ({ onChange, name, value }) => {
  return (
    <Container>
      <label className="checkbox">
        {value}
        <input type="checkbox" onChange={onChange} name={name} value={value} />
        <span className="checkbox__text" />
      </label>
    </Container>
  )
}

export default Checkbox
