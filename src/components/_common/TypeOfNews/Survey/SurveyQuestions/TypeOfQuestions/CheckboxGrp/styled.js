import styled from 'styled-components'

export const Container = styled.div`
  margin-bottom: 15px;
  & .checkbox-group-header-text {
    color: #1b1e2d;
    font-family: PFEncoreSansPro-Medium;
    font-size: 14px;
    line-height: 20px;
    opacity: 0.97;
    margin-bottom: 10px;
  }
`
