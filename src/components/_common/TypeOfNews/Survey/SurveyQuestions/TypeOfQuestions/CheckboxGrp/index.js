import React from 'react'
import * as _ from 'lodash'

import Checkbox from './Checkbox'

import { Container } from './styled'

/* type SurveyBodyProps = {
  questions: Array<any>,
} */

const CheckboxGrp = ({ input, questionData, questionNumber }) => {
  const allOptions = questionData.options.map((option, i) => (
    <Checkbox
      key={i}
      index={i}
      value={option}
      name={`checkboxGrp${questionNumber + 1}`}
      onChange={({ target }) => {
        const values = [...input.value]

        if (values.indexOf(target.value) === -1) {
          values.push(target.value)
        } else {
          _.pull(values, target.value)
        }

        input.onChange(values)
      }}
    />
  ))
  return (
    <Container>
      <div className="checkbox-group-header-text">{`${questionNumber + 1}. ${
        questionData.text
      }`}</div>
      <div>{allOptions}</div>
    </Container>
  )
}

export default CheckboxGrp
