import styled from 'styled-components'

export const Container = styled.div`
  background-color: #ffffff;
  border: 0.3px solid rgba(210, 210, 210, 0.5099999904632568);
  box-shadow: 0 0 6px 0 rgba(0, 0, 0, 0.09000000357627869);
  width: 704px;
  padding: 20px;
  margin-bottom: 15px;

  & .news_opened {
    background-color: rgba(0, 0, 0, 0.44999998807907104);
    width: 280px;
    height: 139px;
  }
  & .preview-wrapper_news-closed {
    height: 140px;
  }

  & .preview-wrapper {
    display: flex;
    align-items: start;
    justify-content: space-between;
  }

  & .preview-text_with-image {
    width: 44%;
  }
`
