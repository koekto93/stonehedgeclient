import React from 'react'

import { Container } from './styled'

type NotificationControlProps = {
  type: string,
  date?: string,
}

const NewsControl = ({ type, date }: NotificationControlProps) => {
  return (
    <Container>
      <div className="notification-control-type">
        <span className="notification-control-type__value">{type}</span>
      </div>
      <div className="notification-control-date">
        <span className="notification-control-date__value">{date}</span>
      </div>
    </Container>
  )
}

export default NewsControl
