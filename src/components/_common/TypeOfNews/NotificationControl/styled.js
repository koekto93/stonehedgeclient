import styled from 'styled-components'

export const Container = styled.div`
  display: flex;
  flex-flow: column nowrap;
  align-items: center;
  justify-content: space-between;
  height: 54%;

  & .notification-control-type {
    border: 1px solid #68d88a;
    border-radius: 5px;
    width: 85px;
    height: 18px;
    display: flex;
    align-items: center;
    justify-content: center;
    margin-bottom: 20px;

    & .notification-control-type__value {
      font-size: 10px;
      color: #68d88a;
      font-family: PFEncoreSansPro-Regular;
    }
  }

  & .notification-control-date__value {
    color: #1b1e2d;
    font-size: 12px;
  }

  & .notification-control-date {
    font-size: 12px;
  }
`
