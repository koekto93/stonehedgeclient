import styled from 'styled-components'

export const Container = styled.div`
  p {
    margin: 0;
  }

  & .preview-text-theme {
    color: #1b1e2d;
    font-family: PFEncoreSansPro-Medium;
    font-size: 16px;
    line-height: 20px;
    margin-bottom: 18px;
  }

  & .preview-text-short-text {
    color: #1b1e2d;
    font-family: PFEncoreSansPro-Book;
    font-size: 14px;
    font-weight: 300;
    letter-spacing: 0.18px;
    line-height: 20px;
  }
`
