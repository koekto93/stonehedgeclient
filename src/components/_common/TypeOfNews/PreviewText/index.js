//@flow
import React from 'react'

import { Container } from './styled'

type PreviewTextProps = {
  theme: string,
  shortText: string,
}

const PreviewText = ({ theme, shortText }: PreviewTextProps) => {
  return (
    <Container>
      <p className="preview-text-theme">{theme}</p>
      <p className="preview-text-short-text">{shortText}</p>
    </Container>
  )
}

export default PreviewText
