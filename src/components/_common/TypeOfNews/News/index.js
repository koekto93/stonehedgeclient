//@flow
import React from 'react'

import Image from '../../Image'
import PreviewText from '../PreviewText'
import NewsControl from '../NewsControl'
import NewsFullText from './NewsFullText'

import { Container } from './styled'

type newsDataType = {
  id: string,
  theme: string,
  newsType: string,
  shortText: string,
  fullText: string,
  date: string,
  imgSrc: string,
  image: boolean,
}
type NewsProps = {
  newsData: newsDataType,
}

const openNewsId = '21222111'

const News = ({ newsData }: NewsProps) => {
  return (
    <Container>
      <div
        className={`preview-wrapper ${
          newsData.id === openNewsId
            ? ''
            : newsData.image
              ? 'preview-wrapper_with-image'
              : 'preview-wrapper_without-image'
        }`}
      >
        {newsData.image && (
          <Image
            src={newsData.imgSrc}
            width="280px"
            height="139px"
            alt="surveyImg"
          />
        )}
        <div className={newsData.image ? 'preview-text_with-image' : ''}>
          <PreviewText theme={newsData.theme} shortText={newsData.shortText} />
        </div>
        <NewsControl
          type={newsData.newsType}
          date={newsData.date}
          image={newsData.image}
        />
      </div>
      {openNewsId === newsData.id && (
        <NewsFullText text={newsData.fullText} imgSrc={newsData.imgSrc} />
      )}
    </Container>
  )
}

export default News
