import styled from 'styled-components'

export const Container = styled.div`
  background-color: #ffffff;
  border: 0.3px solid rgba(210, 210, 210, 0.5099999904632568);
  box-shadow: 0 0 6px 0 rgba(0, 0, 0, 0.09000000357627869);
  width: 704px;
  padding: 20px;
  margin-bottom: 15px;

  & .preview-wrapper {
    display: flex;
    align-items: start;
    justify-content: space-between;
  }

  & .preview-wrapper_without-image {
    height: 106px;
  }

  & .preview-wrapper_with-image {
    height: 140px;
  }

  & .preview-text_with-image {
    width: 44%;
  }
`
