import React from 'react'

import Image from '../../../Image'

import { Container } from './styled'

/* type newsDataType = {
  id: string,
  theme: string,
  newsType: string,
  shortText: string,
  fullText: string,
  date: string,
  imgSrc: string,
  image: boolean,
}
type NewsProps = {
  newsData: newsDataType,
} */

const NewsFullText = ({ text, imgSrc }) => {
  return (
    <Container>
      <Image src={imgSrc} width="704px" height="398px" alt="surveyImg" />
      <div
        className="news-full-text"
        dangerouslySetInnerHTML={{ __html: text }}
      />

      <button className="button">МНЕ НРАВИТСЯ</button>
    </Container>
  )
}

export default NewsFullText
