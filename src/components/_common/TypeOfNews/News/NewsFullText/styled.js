import styled from 'styled-components'

export const Container = styled.div`
  margin-top: 20px;
  & .news-full-text {
    color: #1b1e2d;
    font-family: PFEncoreSansPro-Book;
    font-size: 16px;
    letter-spacing: 0.21px;
    line-height: 22px;
    text-align: left;
    margin-bottom: 30px;
  }

  & .button {
    border: 1px solid #aaaaaa;
    border-radius: 4px;
    background-color: #ffffff;
    color: #7c7c7c;
    font-family: PFEncoreSansPro-Book;
    font-size: 12px;
    letter-spacing: 0.53px;
    line-height: 14px;
    padding: 8px 30px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
  }
`
