import React from 'react'

import ArrowIcon from '../../ArrowIcon'

import { Container } from './styled'

type NewsControlProps = {
  type: string,
  date?: string,
  image: boolean,
}

const NewsControl = ({ type, date, image }: NewsControlProps) => {
  return (
    <Container>
      <div
        className={`news-control-container ${
          image ? 'news-control-container_with-image' : ''
        }`}
      >
        <div className="news-control-type">
          <span className="news-control-type__value">{type}</span>
        </div>
        <div className="news-control-date">
          <span className="news-control-date__value">{date}</span>
        </div>
        <button className="news-control-arrow-button">
          <ArrowIcon />
        </button>
      </div>
    </Container>
  )
}

export default NewsControl
