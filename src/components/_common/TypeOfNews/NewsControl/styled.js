import styled from 'styled-components'

export const Container = styled.div`
  & .news-control-container {
    display: flex;
    flex-flow: column nowrap;
    align-items: center;
    justify-content: space-between;
  }

  & .news-control-container_with-image {
    height: 90%;
  }

  & .news-control-type {
    border: 1px solid #061c3f;
    border-radius: 5px;
    width: 85px;
    height: 18px;
    display: flex;
    align-items: center;
    justify-content: center;
    margin-bottom: 20px;

    & .news-control-type__value {
      font-size: 10px;
      font-family: PFEncoreSansPro-Regular;
    }
  }

  & .news-control-date__value {
    color: #1b1e2d;
    font-size: 12px;
  }

  & .news-control-date {
    font-size: 12px;
  }

  & .news-control-arrow-button {
    fill: rgba(27, 30, 45, 0.5099999904632568);
    background-color: transparent;
    border: none;
    outline: none !important;
    width: 70px;
    height: 80px;
  }
`
