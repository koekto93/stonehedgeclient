//@flow
import React from 'react'

import PreviewText from '../PreviewText'
import NotificationControl from '../NotificationControl'

import { Container } from './styled'

type notificationDataType = {
  id: string,
  theme: string,
  newsType: string,
  text: string,
  fullText: string,
  date: string,
  imgSrc: string,
  image: boolean,
}
type NotificationProps = {
  notificationData: notificationDataType,
}

const Notification = ({ notificationData }: NotificationProps) => {
  return (
    <Container>
      <PreviewText
        theme={notificationData.theme}
        shortText={notificationData.text}
      />
      <NotificationControl
        type={notificationData.newsType}
        date={notificationData.date}
      />
    </Container>
  )
}

export default Notification
