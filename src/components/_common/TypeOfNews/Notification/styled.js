import styled from 'styled-components'

export const Container = styled.div`
  background-color: #ffffff;
  border: 0.3px solid rgba(210, 210, 210, 0.5099999904632568);
  box-shadow: 0 0 6px 0 rgba(0, 0, 0, 0.09000000357627869);
  width: 704px;
  height: 106px;
  padding: 20px;
  display: flex;
  align-items: start;
  justify-content: space-between;
  margin-bottom: 15px;
`
