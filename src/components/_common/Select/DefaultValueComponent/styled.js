import styled from 'styled-components'

export const Container = styled.div`
  display: flex;
  align-items: center;
  width: 100%;
  height: 100%;
  font-size: 12px;
  margin-left: 10px;
  font-family: PFEncoreSansPro-Book;
`
