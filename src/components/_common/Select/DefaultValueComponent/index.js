import React from 'react'

import { Container } from './styled'

type DefaultValueComponentProps = {
  value: string,
}

const DefaultValueComponent = (props: DefaultValueComponentProps) => {
  return <Container>{props.value}</Container>
}

export default DefaultValueComponent
