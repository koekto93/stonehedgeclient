import styled from 'styled-components'

export const Container = styled.div`
  background-color: #ffffff;
  border: 0.3px solid rgba(210, 210, 210, 0.5099999904632568);
  box-shadow: 0 0 4px 0 rgba(0, 0, 0, 0.07999999821186066);
  border-radius: 2px;
  width: 146px;
  height: 29px;
  display: flex;

  & .select-value {
    width: 80%;
    height: 100%;
    cursor: pointer;
  }

  & .select-arrow-button {
    width: 20%;
    height: 100%;
    fill: rgba(27, 30, 45, 0.5099999904632568);
    background-color: transparent;
    border: none;
    outline: none !important;
  }
`
