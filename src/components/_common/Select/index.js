//@flow
import React from 'react'

import ArrowIcon from '../ArrowIcon'
import DefaultValueComponent from './DefaultValueComponent'

import { Container } from './styled'

type SelectProps = {
  value: string,
}

const Select = (props: SelectProps) => {
  return (
    <Container className="select-root">
      <div className="select-value">
        <DefaultValueComponent value={props.value} />
      </div>
      <button className="select-arrow-button">
        <ArrowIcon />
      </button>
    </Container>
  )
}

export default Select
