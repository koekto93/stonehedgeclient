import React, { Component } from 'react'

import NewsFeed from './NewsFeed'

import { Container } from './styled'

export class MainPage extends Component {
  render() {
    return (
      <Container>
        <NewsFeed />
      </Container>
    )
  }
}

export default MainPage
