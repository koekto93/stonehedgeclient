import styled from 'styled-components'

export const Container = styled.div`
  width: 704px;

  & .news-feed-label {
    font-family: PFEncoreSansPro-Light;
    font-size: 38px;
    line-height: 46px;
    color: #1b1e2d;
    margin-bottom: 20px;
  }

  & .news-feed-filter {
    margin-bottom: 20px;
  }
`
