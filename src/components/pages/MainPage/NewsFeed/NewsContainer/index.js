import React from 'react'

import Survey from '../../../../_common/TypeOfNews/Survey'
import News from '../../../../_common/TypeOfNews/News'
import Notification from '../../../../_common/TypeOfNews/Notification'

import { Container } from './styled'

import { surveyData, newsData, notificationData } from '../../../../../data'

const NewsContainer = () => {
  const surveyNews = surveyData.map(news => (
    <Survey newsData={news} key={news.id} />
  ))
  const usualNews = newsData.map(news => <News newsData={news} key={news.id} />)
  const notifications = notificationData.map(notification => (
    <Notification notificationData={notification} key={notification.id} />
  ))
  return (
    <Container>{[...surveyNews, ...usualNews, ...notifications]}</Container>
  )
}

export default NewsContainer
