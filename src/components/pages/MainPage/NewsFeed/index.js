import React from 'react'

import Filter from './Filter'
import NewsContainer from './NewsContainer'

import { Container } from './styled'

const NewsFeed = () => {
  return (
    <Container>
      <div className="news-feed-label">
        <span>Моя лента</span>
      </div>
      <div className="news-feed-filter">
        <Filter />
      </div>
      <NewsContainer />
    </Container>
  )
}

export default NewsFeed
