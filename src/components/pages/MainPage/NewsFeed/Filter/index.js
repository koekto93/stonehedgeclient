import React from 'react'

import Select from '../../../../_common/Select'

import { Container } from './styled'

/* const items = ['ВСЕ', 'НОВОСТИ', 'УВЕДОМЛЕНИЯ'] */

const Filter = () => {
  return (
    <Container>
      <Select value="ВСЕ" />
    </Container>
  )
}

export default Filter
