// @flow
import axios from 'axios'

export const apiGetTrips = () =>
  axios.get('/api/trips').then(({ data }) => data)
